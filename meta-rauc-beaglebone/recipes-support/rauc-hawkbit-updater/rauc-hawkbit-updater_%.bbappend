FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit systemd useradd

SRC_URI += "file://rauc-hawkbit-updater.conf \
            file://rauc-hawkbit-updater-setup.sh" 


do_install_append () {
        install -m 644 ${WORKDIR}/rauc-hawkbit-updater.conf ${D}${sysconfdir}/rauc-hawkbit-updater/config.conf
        install -m 777 ${WORKDIR}/rauc-hawkbit-updater-setup.sh ${D}${bindir}/rauc-hawkbit-updater-setup.sh
	sed -i "s/ExecStart.*/ExecStart=\/usr\/bin\/rauc-hawkbit-updater-setup.sh/g" ${D}${systemd_unitdir}/system/rauc-hawkbit-updater.service

}
