#! /bin/sh

serial=`cat /proc/cpuinfo | grep Serial | cut -d' ' -f2-`
hardware=`cat /proc/cpuinfo | grep Hardware | cut -d' ' -f2-`
revision=`cat /proc/cpuinfo | grep Revision | cut -d' ' -f2-`
mac=`cat /sys/class/net/*/address | tail -n1`


echo ${serial} ${hardware} ${revsion} ${mac}

sed "s/test-target/BBB-${serial}/g" /etc/rauc-hawkbit-updater/config.conf > /tmp/rauc-hawkbit-updater.conf
sed -i "s/ff:ff:ff:ff:ff:ff/${mac}/g" /tmp/rauc-hawkbit-updater.conf

/usr/bin/rauc-hawkbit-updater -s -c /tmp/rauc-hawkbit-updater.conf
