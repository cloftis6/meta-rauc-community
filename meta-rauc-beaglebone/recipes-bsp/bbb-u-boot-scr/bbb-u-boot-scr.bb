SUMMARY = "U-boot boot scripts for BBB" 
LICENSE = "MIT" 
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "u-boot-mkimage-native"
inherit deploy

SRC_URI = "file://boot.cmd.in"

do_compile() {
    echo "~~~~~u-boot-cmd"
    mkimage -A arm -T script -C none -n "Boot script" -d "${WORKDIR}/boot.cmd.in" "${WORKDIR}/boot.scr"
    touch "${WORKDIR}/testfile"
}

do_deploy() {
    install -d ${DEPLOYDIR}
    echo "~~~~~u-boot-cpy"
    install -m 0644 "${WORKDIR}/boot.scr" ${DEPLOYDIR}
}

addtask do_deploy after do_compile before do_build

PROVIDES += "u-boot-default-script"
IMAGE_BOOT_FILES_append = " boot.scr"


