FILESEXTRAPATHS_prepend_bbb := "${THISDIR}/files:"
SRC_URI_append_bbb = " file://beaglebone-rauc.rules"

do_install_append_bbb() {
    install -m 0644 ${WORKDIR}/beaglebone-rauc.rules ${D}${sysconfdir}/udev/mount.blacklist.d/
}
